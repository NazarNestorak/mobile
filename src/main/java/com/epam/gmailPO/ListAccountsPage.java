package com.epam.gmailPO;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class ListAccountsPage extends PageObject{

    @FindBy(id = "com.google.android.gm:id/action_done")
    WebElement confirmAccountButton;

    public WebElement getConfirmAccountButton() {
        return confirmAccountButton;
    }
}
