package com.epam.gmailPO;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class WelcomePage extends PageObject{
    @FindBy(xpath = "//[@resource-id='com.google.android.gm:id/gm_dismiss_button']")
    WebElement buttonNext;

    public WebElement getButtonNext() {
        return buttonNext;
    }
}
