package com.epam.gmailPO;

import com.epam.helper.DriverProvider;
import org.openqa.selenium.support.PageFactory;

public class PageObject {

    public PageObject() {
        PageFactory.initElements(DriverProvider.getDriver(),this);
    }
}

