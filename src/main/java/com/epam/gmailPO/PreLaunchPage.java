package com.epam.gmailPO;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class PreLaunchPage extends PageObject{
    @FindBy(id = "com.google.android.gm:id/welcome_tour_got_it")
    WebElement buttonIsClear;

    public WebElement getButtonIsClear() {
        return buttonIsClear;
    }
}


