package com.epam.gmailPO;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class CreateNewMessagePage {

    @FindBy(xpath = "//*[@resource-id='com.google.android.gm:id/to_heading']")
    WebElement recipientField;

    @FindBy(xpath = "//*[@resource-id='com.google.android.gm:id/subject']")
    WebElement subjectField;

    @FindBy(xpath = "//*[@resource-id='com.google.android.gm:id/wc_body_layout']")
    WebElement textField;

    @FindBy(xpath = "//*[@resource-id='com.google.android.gm:id/send']")
    WebElement sendMessageButton;

    public WebElement getRecipientField() {
        return recipientField;
    }

    public WebElement getSubjectField() {
        return subjectField;
    }

    public WebElement getTextField() {
        return textField;
    }

    public WebElement getSendMessageButton() {
        return sendMessageButton;
    }
}
