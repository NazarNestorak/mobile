package com.epam.gmailBO;

import com.epam.gmailPO.ListAccountsPage;
import com.epam.gmailPO.PreLaunchPage;
import com.epam.gmailPO.WelcomePage;
import com.epam.helper.Waiter;
import com.epam.model.Letter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebElement;

public class GmailPageBO {
    private static Logger logger = LogManager.getLogger(GmailPageBO.class);
    private PreLaunchPage preLaunchPage;
    private ListAccountsPage listAccountsPage;
    private WelcomePage welcomePage;

    private Letter letter;

    public GmailPageBO() {
        letter = new Letter();
        preLaunchPage = new PreLaunchPage();
        listAccountsPage = new ListAccountsPage();
        welcomePage = new WelcomePage();
    }



    public GmailPageBO logging() {
//        logger.info("Fill in the 'user login' field ");
        preLaunchPage.getButtonIsClear().click();
//        logger.info("Pressing the button to confirm 'user login' and switching to 'PasswordPage'");
        listAccountsPage.getConfirmAccountButton().click();
//        logger.info("Fill in the 'user password' field ");
        welcomePage.getButtonNext().click();
//        logger.info("Pressing the button to confirm 'user password' and switching to 'GmailHomePage'");
        return this;
    }

//    public void writeAndSendLetter(Letter letter) {
//        logger.info("Click 'Compose' button");
//        gmailHomePage.getComposeButton().clickIfButtonClickable();
//        logger.info("Fill in field 'Recipient'");
//        gmailHomePage.getFieldRecipient().sendKeys(letter.getRecipient());
//        logger.info("Fill in field 'Subject'");
//        gmailHomePage.getFieldSubject().sendKeys(letter.getSubject());
//        logger.info("Fill in field 'Text'");
//        gmailHomePage.getTextField().sendKeys(letter.getText());
//        logger.info("Click 'Send Message' button");
//        gmailHomePage.getSendLetterButton().clickIfButtonClickable();
//    }
//
//    public void openLastSendMessage() throws InterruptedException {
//        logger.info("Open folder with all sent message");
//        gmailHomePage.getSendFolder().clickIfButtonClickable();
//        logger.info("Select the message that was last sent");
////        Thread.sleep(5000);
//        Waiter.waitForElement(gmailHomePage.getLastSentMessage()).click();
////        gmailHomePage.getLastSentMessage().click();
//    }
//
//    public String  getTitle() {
//        logger.info("Get 'letter title' from message");
//        return gmailHomePage.getLetterTitle().getText();
//    }

}
