package com.epam.helper;

import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotInteractableException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static com.epam.helper.Constanta.WAIT_SLEEP_TIME;
import static com.epam.helper.Constanta.WAIT_TIME_OUT;

public class Waiter {

    public Waiter() {
    }

    public static WebElement waitForElementPresent(WebElement element) {
        return new WebDriverWait(DriverProvider.getDriver(), WAIT_TIME_OUT, WAIT_SLEEP_TIME )
                .ignoring(StaleElementReferenceException.class, ElementNotInteractableException.class)
                .until(ExpectedConditions.elementToBeClickable(element));
    }

    public static WebElement waitForElement(WebElement item) {
        WebDriverWait wait = new WebDriverWait(DriverProvider.getDriver(), 90);
        WebElement element = wait.until(ExpectedConditions.elementToBeClickable(item));
        return element;
    }
}
