package com.epam.helper;

public class Constanta {
    public static final  int WAIT_TIME_OUT = 90;
    public static final  int WAIT_SLEEP_TIME = 1500;
    public static final String RECIPIENT = "nestoraknazik@gmail.com";
    public static final String SUBJECT = "Argent letter";
    public static final String TEXT = "It is trial letter";
    public static final String REMOTE_DRIVER_URL = "http://127.0.0.1:4723/wd/hub";

    private Constanta() {
    }
}
