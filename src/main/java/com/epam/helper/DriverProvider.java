package com.epam.helper;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

import static com.epam.helper.Constanta.REMOTE_DRIVER_URL;

public class DriverProvider {
    private final static int TIMEOUT = 90;
    private static DesiredCapabilities capabilities = new DesiredCapabilities();
    private static WebDriver driver;

    public DriverProvider() {
    }

    public static WebDriver getDriver() {
        if (Objects.isNull(driver)) {
            getRemoteDriver();
            setUpDriver();
        }
        return driver;
    }

    private static void getRemoteDriver() {
        try {
            driver = new RemoteWebDriver(new URL(REMOTE_DRIVER_URL), capabilities);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    private static void setUpDriver() {
        driver.manage().timeouts().implicitlyWait(TIMEOUT, TimeUnit.SECONDS);
    }

    public static void closeDriver() {
        if (!Objects.isNull(driver)) {
            driver.quit();
        }
    }

    /*
    public static WebDriver getDriver() {
        if (Objects.isNull(driver)) {
            try {
                driver = new RemoteWebDriver(new URL("http://127.0.0.1:4723/wd/hub"), capabilities );
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            driver.manage().timeouts().implicitlyWait(TIMEOUT, TimeUnit.SECONDS);
        }
        return driver;
    }
    */
}
