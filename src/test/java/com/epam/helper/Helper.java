package com.epam.helper;

public class Helper {

    public static final String WEBDRIVER = "webdriver.chrome.driver";
    public static final String WEBDRIVER_PATH = "src/main/resources/chromedriver.exe";
    public static final String GMAIL_LOGIN_PAGE_URL = "https://accounts.google.com/ServiceLogin/signinchooser?service=mail&passive=true&rm=false&continue=https%3A%2F%2Fmail.google.com%2Fmail%2F&ss=1&scc=1&ltmpl=default&ltmplcache=2&emr=1&osid=1&flowName=GlifWebSignIn&flowEntry=ServiceLogin";
    public static final String PASSWORD = "may the force be with you";
    public static final String LOGIN = "nazarnestorak@gmail.com";
    public static final String LOGIN_1 = "nazarnestorak1@gmail.com";
    public static final String LOGIN_2 = "nazarnestorak2@gmail.com";
    public static final String LOGIN_3 = "nazarnestorak3@gmail.com";

    public Helper() {
    }
    
}
